# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-libs/igraph/igraph-0.6.4.ebuild,v 1.4 2013/03/01 10:44:58 jlec Exp $

EAPI=5

DESCRIPTION="Creating and manipulating undirected and directed graphs"
HOMEPAGE="http://igraph.sourceforge.net/index.html"
SRC_URI="mirror://sourceforge/project/${PN}/C%20library/${PV}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="debug gmp static-libs"

RDEPEND="
	dev-libs/libxml2
	dev-libs/gmp"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

src_configure() {
	econf
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
}
